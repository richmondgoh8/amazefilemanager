package com.amaze.filemanager.utils.files;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.amaze.filemanager.fragments.MainFragment;
import com.amaze.filemanager.utils.OpenMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Richmond Goh on 02-09-2018.
 * Inspired from https://www.journaldev.com/957/java-zip-file-folder-example
 */

public class CompressUtil {

    private static final int BUFFER_SIZE = 2048;
    private static final String TAG = "Compression";
    private static boolean isZipped;
    //
    public static void zipFile(String path, final OpenMode openMode, final Context context, MainFragment m)
    {
        Log.d(TAG, "Zip Request Received");
        new AsyncTask<String, Void, String>()
        {
            @Override
            protected String doInBackground(String... params) {
                try {
                    isZipped = false;
                    String filePath = params[0];; //path to file
                    File fileBinary = new File(filePath); // get file contents
                    //create ZipOutputStream to write to the zip file
                    String targetPath = stripExtension(filePath)+".zip";

                    if (fileExists(targetPath))
                    {
                        return isZipped + "";
                    }

                    FileOutputStream fos = new FileOutputStream(targetPath);

                    Log.d(TAG, "Retrieving Info... ");


                    ZipOutputStream zos = new ZipOutputStream(fos);
                    //add a new Zip Entry to the ZipOutputStream
                    ZipEntry ze = new ZipEntry(fileBinary.getName());
                    zos.putNextEntry(ze);
                    //read the file and write to ZipOutputStream
                    FileInputStream fis = new FileInputStream(fileBinary);
                    byte[] buffer = new byte[1024];
                    int len;
                    while ((len = fis.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                    //Close the zip entry to write to zip file
                    zos.closeEntry();
                    //Close resources
                    zos.close();
                    fis.close();
                    fos.close();
                    Log.d(TAG, fileBinary.getCanonicalPath()+" is zipped to "+ filePath+".zip");

                    isZipped = true;

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return isZipped + "";
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                Log.d(TAG, "Message Receieved" + result);
                if (result.equals(true+""))
                {
                    Log.d(TAG, "Success " + result);
                    Toast.makeText(context, "Zip File Created!", Toast.LENGTH_LONG).show();
                } else {
                    Log.d(TAG, "Failure " + result);
                    Toast.makeText(context, "Oops, File already exist!", Toast.LENGTH_LONG).show();


                }
                m.updateList();
            }

        }.execute(path);
    }

    public static boolean fileExists(String filename) {
        File file = new File(filename);
        if(file == null || !file.exists()) {
            return false;
        }
        return true;
    }

    static String stripExtension (String str) {
        // Handle null case specially.

        if (str == null) return null;

        // Get position of last '.'.

        int pos = str.lastIndexOf(".");

        // If there wasn't any '.' just return the string as is.

        if (pos == -1) return str;

        // Otherwise return the string, up to the dot.

        return str.substring(0, pos);
    }
}


