package com.amaze.filemanager.utils.files;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.amaze.filemanager.utils.OpenMode;

import org.json.JSONObject;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Richmond Goh on 02-09-2018.
 * API Reference - https://www.file.io
 */

public class UploadUtil {

    private static String secretURL = "https://file.io/?expires=1d"; // by default is 14 days or when someone has downloaded the file
    private static String generatedURL = "";

    public static void uploadFile(String path, final OpenMode openMode, final Context context)
    {
        Log.d("UploadState", "Request Received");
        new AsyncTask<String, Void, String>()
        {
            @Override
            protected String doInBackground(String... params) {
                try {

                    String url = secretURL;
                    String shareFilePath = params[0];; //path to file
                    File toUploadFile = new File(shareFilePath); // get file contents

                    OkHttpClient.Builder builder = new OkHttpClient.Builder();
                    builder.connectTimeout(30, TimeUnit.SECONDS);
                    builder.readTimeout(30, TimeUnit.SECONDS);

                    RequestBody requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("file", shareFilePath,
                                    RequestBody.create(MediaType.parse("application/octet-stream"), toUploadFile)) // accept all binary files
                            .build();

                    Request requestBuilder = new Request.Builder()
                            .url(url)
                            .post(requestBody)
                            .build();

                    OkHttpClient client = builder.build();

                    Response response = client.newCall(requestBuilder).execute();
                    String respMsg = response.body().string();
                    JSONObject jsonObj = new JSONObject(respMsg);
                    generatedURL = jsonObj.getString("link");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return generatedURL;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (!result.equals(""))
                {
                    Log.d("UploadState", "Success " + result);
                    FileUtils.copyToClipboard(context, result);
                    Toast.makeText(context, "Shareable Link Copied!", Toast.LENGTH_LONG).show();
                } else {
                    Log.d("UploadState", "Failure " + result);
                    Toast.makeText(context, "Oops, Something went wrong", Toast.LENGTH_LONG).show();
                }

            }

        }.execute(path);
    }
}