package com.amaze.filemanager.utils.files;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.amaze.filemanager.fragments.MainFragment;
import com.amaze.filemanager.utils.OpenMode;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

/**
 * Created by Richmond Goh on 02-09-2018.
 * API Reference - https://developers.itextpdf.com/content/itext-7-jump-start-tutorial
 */

public class ConversionUtil {

    private static boolean imgConverted;
    private static final String TAG = "Conversion";

    public static void convertImage(String path, final OpenMode openMode, final Context context, MainFragment m)
    {
        Log.d(TAG, "Request Received");
        new AsyncTask<String, Void, String>()
        {
            @Override
            protected String doInBackground(String... params) {
                try {
                    imgConverted = true;
                    Document document = new Document();
                    String imagePath = params[0];; //path to file

                    String input = imagePath; // path to image file
                    String output = CompressUtil.stripExtension(imagePath) + ".pdf";

                    FileOutputStream fos = new FileOutputStream(output);
                    PdfWriter writer = PdfWriter.getInstance(document, fos);
                    writer.open();
                    document.open();
                    Image img = Image.getInstance(input);
                    float documentWidth = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
                    float documentHeight = document.getPageSize().getHeight() - document.topMargin() - document.bottomMargin();
                    img.scaleToFit(documentWidth, documentHeight);
                    document.add(img);
                    document.close();
                    writer.close();

                } catch (Exception e) {
                    imgConverted = false;
                    e.printStackTrace();
                }

                return imgConverted + "";
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                Log.d(TAG, "Message Receieved" + result);
                if (result.equals(true+""))
                {
                    Log.d(TAG, "Success " + result);
                    Toast.makeText(context, "Image Successfully Converted!", Toast.LENGTH_LONG).show();
                } else {
                    Log.d(TAG, "Failure " + result);
                    Toast.makeText(context, "Oops, Something Went Wrong!", Toast.LENGTH_LONG).show();

                }
                m.updateList();
            }

        }.execute(path);
    }

}

/*
    public static void main(String ... args) {
        Document document = new Document();
        String input = "c:/temp/capture.png"; // .gif and .jpg are ok too!
        String output = "c:/temp/capture.pdf";
        try {
            FileOutputStream fos = new FileOutputStream(output);
            PdfWriter writer = PdfWriter.getInstance(document, fos);
            writer.open();
            document.open();
            document.add(Image.getInstance(input));
            document.close();
            writer.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
*/